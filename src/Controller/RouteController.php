<?php

namespace App\Controller;

use App\Entity\AirportBusBoardingCard;
use App\Entity\FlightBoardingCard;
use App\Entity\TrainBoardingCard;
use App\Service\RouteService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class RouteController
 * @package App\Controller
 *
 * @Route("/route", methods={"POST"})
 */
class RouteController extends BaseController
{
    /**
     * @var EntityManagerInterface $manager
     */
    private $manager;

    /**
     * @var RouteService $routeService
     */
    private $routeService;

    public function __construct(EntityManagerInterface $manager, RouteService $routeService)
    {
        $this->manager      = $manager;
        $this->routeService = $routeService;
    }

    public function __invoke(Request $request) : Response
    {
        $data = $this->getContent($request);

        $boardingCards = array();

        if( isset($data['buses']) ) {
            $boardingCards = $this->createAirportBusBoardingCards($data['buses'], $boardingCards);

            if($boardingCards instanceof JsonResponse) {
                return $boardingCards;
            }
        }

        if( isset($data['trains']) ) {
            $boardingCards = $this->createTrainBoardingCards($data['trains'], $boardingCards);

            if($boardingCards instanceof JsonResponse) {
                return $boardingCards;
            }
        }

        if( isset($data['flights']) ) {
            $boardingCards = $this->createFlightBoardingCards($data['flights'], $boardingCards);

            if($boardingCards instanceof JsonResponse) {
                return $boardingCards;
            }
        }

        $route = $this->routeService->generateRoute($boardingCards);

        if(isset($route['message'])) {
            return $this->customArrayToResponse(
                array(
                    'hasError' => true,
                    'errors' => $route
                )
            );
        }

        else return $this->customArrayToResponse(
            array(
                'hasError' => false,
                'results' => array(
                    'message' => $route
                )
            )
        );
    }

    /**
     * Function to create the airport bus boarding cards from a given request
     *
     * @param array $buses
     * @param array $boardingCards
     * @return mixed
     */
    private function createAirportBusBoardingCards($buses, &$boardingCards)
    {
        foreach ($buses as $bus) {
            $airportBusBoardingCard = $this->createEntity('airportBus', $bus, ['boardingLocation', 'destination']);

            if ($airportBusBoardingCard instanceof JsonResponse) {
                return $airportBusBoardingCard;
            }

            $boardingCards[] = $airportBusBoardingCard;
        }

        return $boardingCards;
    }

    /**
     * Function to create the flight boarding cards from a given request
     *
     * @param $flights array
     * @param $boardingCards array
     * @return array|mixed
     */
    private function createFlightBoardingCards($flights, &$boardingCards)
    {
        foreach ($flights as $flight) {
            $flightBoardingCard = $this->createEntity('flight', $flight, ['boardingLocation', 'destination', 'seatAssignment', 'flightNumber', 'gateNumber']);

            if ($flightBoardingCard instanceof JsonResponse) {
                return $flightBoardingCard;
            }

            $boardingCards[] = $flightBoardingCard;
        }

        return $boardingCards;
    }

    /**
     * Function to create the train boarding cards from a given request
     *
     * @param $trains array
     * @param $boardingCards array
     * @return array|mixed
     */
    private function createTrainBoardingCards($trains, &$boardingCards)
    {
        foreach($trains as $train) {
            $trainBoardingCard = $this->createEntity('train', $train, ['boardingLocation', 'destination', 'trainNumber']);

            if($trainBoardingCard instanceof JsonResponse) {
                return $trainBoardingCard;
            }

            $boardingCards[] = $trainBoardingCard;
        }

        return $boardingCards;
    }

    /**
     * This function checks all the required fields for a certain class
     * e.g: a train boarding ticket is required to have a start and a destination
     * and also a train number, so the boarding card will be the train boarding card
     * and the fields would be boarding location, destination and number
     *
     * @param array $fields
     * @param array $boardingCard
     *
     * @return bool
     */
    private function checkRequiredFields($boardingCard, $fields) {

        foreach($fields as $field) {
            if( !isset($boardingCard[$field]) ) {
                return false;
            }
        }

        return true;
    }

    /**
     * This function creates an entity of type BoardingCard from a given object and some required fields
     * Moreover, this is a generic function, so every time a new mean of transportation is added, it will only require
     * small adjustments to the code.
     *
     * @param $type string
     * @param $object array
     * @param $requiredFields array
     * @return mixed
     */
    private function createEntity($type, $object, $requiredFields)
    {
        if( !$this->checkRequiredFields($object, $requiredFields) ) {
            return $this->customArrayToResponse(
                array (
                    'hasError' => true,
                    'errors' => array (
                        'message' => "Wrong information for a/an $type! Please review your boarding tickets!"
                    )
                )
            );
        }

        $entityName = 'App\\Entity\\' . ucfirst($type) . 'BoardingCard';

        $entity = new $entityName();

        foreach($object as $key => $value) {
            $setter = 'set' . ucfirst($key);
            $entity->$setter($value);
        }

        return $entity;
    }
}