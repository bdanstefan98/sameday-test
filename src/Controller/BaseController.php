<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;

abstract class BaseController extends AbstractController
{
    protected function ok(): Response
    {
        return new JsonResponse();
    }

    protected function getContent(Request $request): array
    {
        $content = $request->getContent();
        return @json_decode($content, true) ?: [];
    }

    protected function customArrayToResponse(array $customArray, $status = null): Response
    {
        $response = new JsonResponse();
        if ($customArray['hasError']) {
            $response->setStatusCode($status ?? Response::HTTP_BAD_REQUEST);
            $response->setContent(json_encode($customArray['errors']));
        } else {
            $response->setStatusCode(Response::HTTP_OK);
            $response->setContent(json_encode($customArray['results']));
        }

        return $response;
    }

    protected function constraintViolationListResponse(ConstraintViolationListInterface $violationList): Response
    {
        $resErrors = [];
        /** @var ConstraintViolationInterface $error */
        foreach ($violationList as $error) {
            if (!isset($resErrors[$error->getPropertyPath()])) {
                $resErrors[$error->getPropertyPath()] = [];
            }

            $resErrors[$error->getPropertyPath()][] = $error->getMessage();
        }

        return $this->customArrayToResponse([
            'hasError' => true,
            'errors' => $resErrors,
        ]);
    }
}