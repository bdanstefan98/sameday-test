<?php


namespace App\Service;


use App\Entity\AirportBusBoardingCard;
use App\Entity\BoardingCard;
use App\Entity\FlightBoardingCard;
use App\Entity\TrainBoardingCard;

class RouteService
{
    /**
     * Main function that communicates with the controller
     *
     * @param $boardingCards array
     * @return array|int|string
     */
    public function generateRoute($boardingCards)
    {
        $helpingMap = array();
        $nodeWeights = array();

        $this->generateHelpMap($boardingCards, $helpingMap, $nodeWeights);

        $location = $this->getStartLocationFromNodeWeights($nodeWeights);

        if( isset($location['message']) ) {
            return $location;
        }

        return $this->orderBoardingTickets($helpingMap, $location);
    }

    /**
     * This function will allow us to determine the start and the end of the journey
     * whilst also map the tickets by their boarding location, helping us in order to
     * determine the correct order of the tickets.
     *
     * The array $nodeWeights is built in order
     * to determine the start and end by checking the number of occurrences a city has
     * in all of the tickets. If it's only one, it means that it's either a start or an
     * end, based on its position on the ticket (boardingLocation or destination)
     *
     * The array $helpingMap will basically gain us access to O(1) in terms of accessing
     * a certain boarding ticket. By mapping the tickets by their boarding location, we
     * can easily obtain information on the next ticket's start based on the current one's
     * end just by accessing $helpingMap[$currentTicket->getDestination()]
     *
     *
     * @param $boardingCards array<BoardingCard>
     * @param $helpingMap array
     * @param $nodeWeights array
     */
    private function generateHelpMap($boardingCards, &$helpingMap, &$nodeWeights)
    {
        /** @var BoardingCard $boardingCard */
        foreach($boardingCards as $boardingCard) {
            $helpingMap[$boardingCard->getBoardingLocation()] = $boardingCard;

            if(isset($nodeWeights[$boardingCard->getDestination()])) {
                $nodeWeights[$boardingCard->getDestination()]['weight']++;
            } else {
                $nodeWeights[$boardingCard->getDestination()] = [
                    'weight' => 1,
                    'zone' => 'destination'
                ];

            }

            if(isset($nodeWeights[$boardingCard->getBoardingLocation()])) {
                $nodeWeights[$boardingCard->getBoardingLocation()]['weight']++;
            } else {
                $nodeWeights[$boardingCard->getBoardingLocation()] = [
                    'weight' => 1,
                    'zone' => 'start'
                ];
            }
        }
    }

    /**
     * This function will return the start of the journey based on the previous mapping
     *
     * @param $nodeWeights
     * @return array|int|string
     */
    private function getStartLocationFromNodeWeights($nodeWeights)
    {
        foreach ($nodeWeights as $location => $nodeWeight) {
            if($nodeWeight['weight'] == 1 && $nodeWeight['zone'] == 'start') {
                return $location;
            }
        }

        return array(
            'message' => "The journey's start could not be determined!"
        );
    }

    /**
     * This is our main function of ordering boarding tickets implemented as described above
     *
     * @param $helpingMap
     * @param $start
     * @return string
     */
    private function orderBoardingTickets($helpingMap, $start)
    {
        $sequence = "";
        $destination = $start;

        while ( isset($helpingMap[$destination]) ) {
            if($helpingMap[$destination] instanceof AirportBusBoardingCard) {
                $sequence .= $this->generateAirportBusCardsText($helpingMap[$destination]);
            }

            if($helpingMap[$destination] instanceof TrainBoardingCard) {
                $sequence .= $this->generateTrainCardsText($helpingMap[$destination]);
            }

            if($helpingMap[$destination] instanceof FlightBoardingCard) {
                $sequence .= $this->generateFlightCardsText($helpingMap[$destination]);
            }

            $destination = $helpingMap[$destination]->getDestination();
        }

        $sequence .= ". You have arrived at your final destination.";

        return $sequence;
    }

    /**
     * In case of an airport shuttle, this function will generate the text for its equivalent boarding card
     *
     * @param $airportBusBoardingCard AirportBusBoardingCard
     * @return string
     */
    private function generateAirportBusCardsText($airportBusBoardingCard)
    {
        $returnSequence = "Take the airport bus from " .
            $airportBusBoardingCard->getBoardingLocation() .
            " to " .
            $airportBusBoardingCard->getDestination();

        if($airportBusBoardingCard->getSeatAssignment()) {
            $returnSequence .= ". Sit in seat ". $airportBusBoardingCard->getSeatAssignment() ;
        } else {
            $returnSequence .= ". No seat assignment";
        }

        return $returnSequence;
    }

    /**
     * In case of a train, this function will generate the text for its equivalent boarding card
     *
     * @param $trainBoardingCard TrainBoardingCard
     *
     * @return string
     */
    private function generateTrainCardsText($trainBoardingCard)
    {
        $returnSequence = "Take train " .
            $trainBoardingCard->getTrainNumber() .
            " from " .
            $trainBoardingCard->getBoardingLocation() .
            " to " .
            $trainBoardingCard->getDestination();

        if($trainBoardingCard->getSeatAssignment()) {
            $returnSequence .= ". Sit in seat ". $trainBoardingCard->getSeatAssignment() . ". " ;
        } else {
            $returnSequence .= ". No seat assignment";
        }

        return $returnSequence;
    }

    /**
     * In case of a flight, this function will generate the text for its equivalent boarding card
     *
     * @param $flightBoardingCard FlightBoardingCard
     *
     * @return string
     */
    private function generateFlightCardsText($flightBoardingCard)
    {
        if ( !$flightBoardingCard->getBaggageCollect() ) {
            $returnSequence = "No baggage collect would be necessary. Proceed directly to gate";
        } else {
            $returnSequence = "Proceed to pick up point " . $flightBoardingCard->getBaggageCollect() . " in order to pick up your luggage";
        }

        $returnSequence .= ". From " .
            $flightBoardingCard->getBoardingLocation() .
            " take flight " .
            $flightBoardingCard->getFlightNumber() .
            " to " .
            $flightBoardingCard->getDestination() .
            ". Gate " .
            $flightBoardingCard->getGateNumber() .
            ", seat " .
            $flightBoardingCard->getSeatAssignment();

        if( !$flightBoardingCard->getBaggageDrop() ) {
            $returnSequence .= ". Baggage will be automatically transferred from your last leg";
        } else {
            $returnSequence .= ". Baggage drop at ticket counter " . !$flightBoardingCard->getBaggageDrop();
        }

        return $returnSequence;

    }
}