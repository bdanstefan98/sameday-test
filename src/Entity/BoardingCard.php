<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BoardingCardRepository")
 */
abstract class BoardingCard implements BoardingCardInterface
{
    use Timestampable;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $seatAssignment;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $boardingLocation;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $destination;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSeatAssignment(): ?string
    {
        return $this->seatAssignment;
    }

    public function setSeatAssignment(string $seatAssignment): self
    {
        $this->seatAssignment = $seatAssignment;

        return $this;
    }

    public function getBoardingLocation(): ?string
    {
        return $this->boardingLocation;
    }

    public function setBoardingLocation(string $boardingLocation): self
    {
        $this->boardingLocation = $boardingLocation;

        return $this;
    }

    public function getDestination(): ?string
    {
        return $this->destination;
    }

    public function setDestination(string $destination): self
    {
        $this->destination = $destination;

        return $this;
    }
}
