<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TrainBoardingCardRepository")
 */
class TrainBoardingCard extends BoardingCard
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $trainNumber;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTrainNumber(): ?string
    {
        return $this->trainNumber;
    }

    public function setTrainNumber(string $trainNumber): self
    {
        $this->trainNumber = $trainNumber;

        return $this;
    }
}
