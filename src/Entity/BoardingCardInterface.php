<?php


namespace App\Entity;


interface BoardingCardInterface
{
    public function getId();

    public function getSeatAssignment();

    public function setSeatAssignment(string $seatAssignment);

    public function getBoardingLocation();

    public function setBoardingLocation(string $boardingLocation);

    public function getDestination();

    public function setDestination(string $destination);
}