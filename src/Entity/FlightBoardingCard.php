<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FlightBoardingCardRepository")
 */
class FlightBoardingCard extends BoardingCard
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $flightNumber;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $gateNumber;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $baggageDrop;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $baggageCollect;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFlightNumber(): ?string
    {
        return $this->flightNumber;
    }

    public function setFlightNumber(string $flightNumber): self
    {
        $this->flightNumber = $flightNumber;

        return $this;
    }

    public function getGateNumber(): ?string
    {
        return $this->gateNumber;
    }

    public function setGateNumber(string $gateNumber): self
    {
        $this->gateNumber = $gateNumber;

        return $this;
    }

    public function getBaggageDrop(): ?string
    {
        return $this->baggageDrop;
    }

    public function setBaggageDrop(string $baggageDrop): self
    {
        $this->baggageDrop = $baggageDrop;

        return $this;
    }

    public function getBaggageCollect(): ?string
    {
        return $this->baggageCollect;
    }

    public function setBaggageCollect(string $baggageCollect): self
    {
        $this->baggageCollect = $baggageCollect;

        return $this;
    }
}
