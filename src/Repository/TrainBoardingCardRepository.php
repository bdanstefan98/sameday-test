<?php

namespace App\Repository;

use App\Entity\TrainBoardingCard;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TrainBoardingCard|null find($id, $lockMode = null, $lockVersion = null)
 * @method TrainBoardingCard|null findOneBy(array $criteria, array $orderBy = null)
 * @method TrainBoardingCard[]    findAll()
 * @method TrainBoardingCard[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TrainBoardingCardRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TrainBoardingCard::class);
    }

    // /**
    //  * @return TrainBoardingCard[] Returns an array of TrainBoardingCard objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TrainBoardingCard
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
