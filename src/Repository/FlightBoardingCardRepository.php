<?php

namespace App\Repository;

use App\Entity\FlightBoardingCard;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method FlightBoardingCard|null find($id, $lockMode = null, $lockVersion = null)
 * @method FlightBoardingCard|null findOneBy(array $criteria, array $orderBy = null)
 * @method FlightBoardingCard[]    findAll()
 * @method FlightBoardingCard[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FlightBoardingCardRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FlightBoardingCard::class);
    }

    // /**
    //  * @return FlightBoardingCard[] Returns an array of FlightBoardingCard objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FlightBoardingCard
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
