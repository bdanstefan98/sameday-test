<?php

namespace App\Repository;

use App\Entity\BoardingCard;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BoardingCard|null find($id, $lockMode = null, $lockVersion = null)
 * @method BoardingCard|null findOneBy(array $criteria, array $orderBy = null)
 * @method BoardingCard[]    findAll()
 * @method BoardingCard[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BoardingCardRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BoardingCard::class);
    }

    // /**
    //  * @return BoardingCard[] Returns an array of BoardingCard objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BoardingCard
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
