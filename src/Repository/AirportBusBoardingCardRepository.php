<?php

namespace App\Repository;

use App\Entity\AirportBusBoardingCard;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AirportBusBoardingCard|null find($id, $lockMode = null, $lockVersion = null)
 * @method AirportBusBoardingCard|null findOneBy(array $criteria, array $orderBy = null)
 * @method AirportBusBoardingCard[]    findAll()
 * @method AirportBusBoardingCard[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AirportBusBoardingCardRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AirportBusBoardingCard::class);
    }

    // /**
    //  * @return AirportBusBoardingCard[] Returns an array of AirportBusBoardingCard objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AirportBusBoardingCard
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
