<?php

namespace App\Tests;

use App\Service\RouteService;
use PHPUnit\Framework\TestCase;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class EntityTest extends WebTestCase
{
    public function testSomething()
    {
        $this->assertTrue(true);
    }

    public function testWithCorrectRequestNoLoops()
    {
        $client = static::createClient();

        /**
         * This is the request given in order to achieve the desired output from the pdf
         */
        $client->request("POST", "/route", [], [] ,[], "
        {
            \"buses\" : [
                {
                    \"boardingLocation\" : \"Barcelona\",
                    \"destination\" : \"Genova\"
                }
            ],
            \"trains\": [
                {
                    \"boardingLocation\" : \"Madrid\",
                    \"destination\" : \"Barcelona\",
                    \"trainNumber\": \"78A\",
                    \"seatAssignment\": \"45B\"
                }
            ],
            \"flights\": [
                {
                    \"boardingLocation\": \"Genova\",
                    \"destination\" : \"Stockholm\",
                    \"gateNumber\": \"45B\",
                    \"seatAssignment\": \"3A\",
                    \"baggageDrop\": \"344\",
                    \"flightNumber\": \"SK455\"
                },
                {
                    \"boardingLocation\": \"Stockholm\",
                    \"destination\" : \"New York JFK\",
                    \"gateNumber\": \"22\",
                    \"seatAssignment\": \"7B\",
                    \"flightNumber\": \"SK22\"
                }
            ]
        }"
        );

        $this->assertTrue($client->getResponse()->isSuccessful());
    }

    public function testWithACyclicRoadRequest()
    {
        $client = static::createClient();

        /**
         * This is the request given in order to achieve a looped journey
         */
        $client->request("POST", "/route", [], [] ,[], "
        {
            \"buses\" : [
                {
                    \"boardingLocation\" : \"Barcelona\",
                    \"destination\" : \"Genova\"
                }
            ],
            \"trains\": [
                {
                    \"boardingLocation\" : \"Madrid\",
                    \"destination\" : \"Barcelona\",
                    \"trainNumber\": \"78A\",
                    \"seatAssignment\": \"45B\"
                }
            ],
            \"flights\": [
                {
                    \"boardingLocation\": \"Genova\",
                    \"destination\" : \"Stockholm\",
                    \"gateNumber\": \"45B\",
                    \"seatAssignment\": \"3A\",
                    \"baggageDrop\": \"344\",
                    \"flightNumber\": \"SK455\"
                },
                {
                    \"boardingLocation\": \"Stockholm\",
                    \"destination\" : \"Madrid\",
                    \"gateNumber\": \"22\",
                    \"seatAssignment\": \"7B\",
                    \"flightNumber\": \"SK22\"
                }
            ]
        }"
        );

        $this->assertTrue(json_decode($client->getResponse()->getContent(), true)['message'] == "The journey's start could not be determined!");
    }

    public function testNotSufficientFlightInformation()
    {
        $client = static::createClient();

        /**
         * This is the request given in order to achieve a looped journey
         */
        $client->request("POST", "/route", [], [] ,[], "
        {
            \"buses\" : [
                {
                    \"boardingLocation\" : \"Barcelona\",
                    \"destination\" : \"Genova\"
                }
            ],
            \"trains\": [
                {
                    \"boardingLocation\" : \"Madrid\",
                    \"destination\" : \"Barcelona\",
                    \"trainNumber\": \"78A\",
                    \"seatAssignment\": \"45B\"
                }
            ],
            \"flights\": [
                {
                    \"boardingLocation\": \"Genova\",
                    \"destination\" : \"Stockholm\",
                    \"seatAssignment\": \"3A\",
                    \"baggageDrop\": \"344\",
                    \"flightNumber\": \"SK455\"
                },
                {
                    \"boardingLocation\": \"Stockholm\",
                    \"destination\" : \"Madrid\",
                    \"gateNumber\": \"22\",
                    \"seatAssignment\": \"7B\",
                    \"flightNumber\": \"SK22\"
                }
            ]
        }"
        );

        $this->assertStringContainsString("Wrong information", json_decode($client->getResponse()->getContent(), true)['message']);
        $this->assertStringContainsString("flight", json_decode($client->getResponse()->getContent(), true)['message']);
    }
}
