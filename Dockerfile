# Before the start I would like to add some mentions: 

1) I did not save anything in the DB, as I didn't think that'd be necessary

2) Also, I did not make a Journey entity to store all previous journeys 
just for the fact that without a user it would be pretty pointless

3) I did not make a command or something of a predefined input, instead I exposed
the "/route" route to the API, leaving it public and subject to multiple testing.
I actually thought that being given the nature of the application, this approach
will suit it better.


# Installing and making it run

Cloning the project: git clone git@gitlab.com:bdanstefan98/sameday-test.git

Installing the packages used: composer install

Opening the server: php bin/console server:run

Tests: vendor/bin/phpunit tests

Testing the api: open Postman (Insomnia or whatever mock-requests client you prefer)
After the server running, send a POST request to yourhost:yourport/route

Mock data: 
{
	"buses" : [
		{
			"boardingLocation" : "Barcelona",
			"destination" : "Genova"
		}
	],
	"trains": [
		{
			"boardingLocation" : "Madrid",
			"destination" : "Barcelona",
			"trainNumber": "78A",
			"seatAssignment": "45B"
		}
	],
	"flights": [
		
		{
			"boardingLocation": "Stockholm",
			"destination" : "New York JFK",
			"gateNumber": "22",
			"seatAssignment": "7B",
			"flightNumber": "SK22"
		},
		{
			"boardingLocation": "Genova",
			"destination" : "Stockholm",
			"gateNumber": "45B",
			"seatAssignment": "3A",
			"baggageDrop": "344",
			"flightNumber": "SK455"
		}
	]
}

